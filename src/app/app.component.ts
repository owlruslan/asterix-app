import { Component } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";


/**
 * Есть поле на форме. Есть к нему label, рядом с ним рисуется <span>*</span>,
 * есть отдельно валидация на обязательность поля где-то там сделанная.
 * Придумать, че бы такое сделать, чтобы отрисовка звездочки и валидация как-то гарантированно совпадали.
 * Возможно, это надо сделать компонент-обертку над label, возможно,
 * еще какой-нибудь удачный подход где-то встречал.
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  form = this.fb.group({
    field: ['', Validators.required],
  });

  constructor(private fb: FormBuilder) {

    this.form.valueChanges.subscribe(value => {
      console.log(`Form is invalid: %c ${this.form.invalid}`, 'color: #bb0000');
    });
  }
}
